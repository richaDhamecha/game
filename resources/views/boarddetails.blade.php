@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">Fill Details</div>
                    <div class="panel-body">

                    {{Session::put('rows',request('rows'))}}
{{Session::put('columns',request('columns'))}}
{{Session::put('no_of_pieces',request('no_of_pieces'))}}

@if(!Session::has('no_of_pieces'))

    <form action="BoardDetails" method="post">
        {{ csrf_field() }}
        <fieldset style="width: 50%;">
            <legend>Board Details</legend>
            Rows:<input type="number" class="form-control" name="rows" value="{{Session::get('rows',0)}}"><br><br>
            Columns:<input type="number" class="form-control" name="columns" value="{{Session::get('columns',0)}}"><br><br>
        </fieldset>
        <fieldset style="width: 50%;">
            <legend>Piece(s)</legend>
            Number of Pieces:<input type="number" class="form-control" name="no_of_pieces" value="{{Session::get('no_of_pieces',0)}}"><br>
            <input type="submit" class="btn btn-primary" value="Next">
        </fieldset>
    </form>

@else

    <form action="StartGame" method="post">
        {{ csrf_field() }}
        <fieldset style="width: 50%;">
            <legend>Board Details</legend>
            Rows:<input type="number" name="rows" class="form-control" value="{{Session::get('rows',0)}}"><br><br>
            Columns:<input type="number" name="columns" class="form-control" value="{{Session::get('columns',0)}}"><br><br>
        </fieldset>
        <fieldset style="width: 50%;">
            <legend>Piece(s)</legend>
            Number of Pieces:<input type="number"  class="form-control" name="no_of_pieces" value="{{Session::get('no_of_pieces',0)}}"><br>
        </fieldset>
        <fieldset style="width: 50%;">
            <legend>Piece Details</legend>
            @for($i=0;$i<Session::get('no_of_pieces',0);$i++)
                Piece{{$i+1}} Row: <input type="number" class="form-control" name="piece{{$i}}row" required="true"><br><br>
                Piece{{$i+1}} Column:<input type="number" class="form-control" name="piece{{$i}}column" required="true"><br><br>
                Piece{{$i+1}} Moves:<input type="text" class="form-control" name="piece{{$i}}moves" required="true"><br><br>

            @endfor
            <input type="submit" class="btn btn-primary" value="Start Game">
        </fieldset>
    </form>
@endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

