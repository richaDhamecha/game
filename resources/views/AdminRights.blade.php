@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Not Admin</div>
                    <div class="panel-body">
                        You are not an admin so you can not access admin rights.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection