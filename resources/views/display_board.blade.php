@extends('layouts.app')
@section('content')
    <html>
    <head>
        @if($errorMsg!='')
            <script type='text/javascript'>
                alert('{{$errorMsg}}');</script>
        @endif


    </head>
    <body>
    @foreach($boardsPieces as $boardsPiece)
        <h1> Piece{{$boardsPiece->piece_id}} : {{$boardsPiece->commands}}</h1>
    @endforeach

    @if($refresh)
        <script>
            setTimeout(function () {
                location.reload()

            }, 1000);
        </script>
    @endif

    <table border="1">
        @for ($i = 1; $i <= $board->rows; $i++)
            <tr>
                @for ($j = 1; $j <= $board->columns; $j++)
                    <?php $flag = false; ?>

                    @foreach($boardsPieces as $boardsPiece)
                        @if (($i==$boardsPiece->x) && ($j==$boardsPiece->y))
                            <?php $flag = true; ?>
                            @break
                        @else
                            <?php $flag = false; ?>
                        @endif
                    @endforeach

                    @if($flag==true)
                        <td align='center' width='100' height='100'>
                            <img src='{{asset("bitcoin.png")}}' height='60' width='60'
                                 alt='piece'>Piece{{$boardsPiece->piece_id}}
                        </td>

                    @else
                        <td width='100' height='100' align='center'></td>
                    @endif
                @endfor
            </tr>
        @endfor
    </table>
    <br>


    <form action="start" method="post">
        {{ csrf_field() }}
        <input type="submit" value="New Game"><br><br>
    </form>

    <form action="move" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="GameId" value="{{$gameId}}">
        <input type="hidden" name="BoardId" value="{{$board->id}}">
        <input type="submit" name="move" value="Start Moves"><br>

    </form>
    </body>
    </html>
@endsection