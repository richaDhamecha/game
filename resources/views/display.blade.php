@extends('layouts.app')

@section('content')
<html>
<head>
    @if($error)
        <script type='text/javascript'>
            alert('{{$errorMsg}}');</script>
    @endif

        @if($refresh)
            <script>
                setTimeout(function () {
                    location.reload()

                }, 1000);
            </script>
        @endif

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
<table border="1">
    @for ($i = 1; $i <= $rows; $i++)
        <tr>
            @for ($j = 1; $j <= $columns; $j++)
                <?php $flag = false; ?>

                @for($k=0;$k<$noOfPieces;$k++)
                    @if (($i==$piecesRows[$k]) && ($j==$piecesColumns[$k]))
                        <?php $flag = true; ?>
                        @break
                    @else
                        <?php $flag = false; ?>
                    @endif
                @endfor

                @if($flag==true)
                    <td align='center' width='100' height='100'>
                        <img src='{{asset("bitcoin.png")}}' height='60' width='60'
                             alt='piece'>Piece{{$pieceID[$k]}}
                    </td>

                @else
                    <td width='100' height='100' align='center'></td>
                @endif
            @endfor
        </tr>
    @endfor
</table><br>


<form action="BoardDetails" method="post">
    {{ csrf_field() }}
    <input type="submit" class="btn btn-primary" value="New Game"><br><br>
</form>

<form action="MovePiece" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="GameId" value="{{$gameId}}">
    <input type="hidden" name="BoardId" value="{{$boardId}}">
    <input type="hidden" name="noOfPieces" value="{{$noOfPieces}}">
    @for($i=0;$i<$noOfPieces;$i++)
        <input type="hidden" name='BoardPiece[]' value="{{$boardPiece[$i]}}">
    @endfor
    <input type="submit" class="btn btn-primary" name="move" value="Start Moves"><br>

</form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

@endsection