<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::any('BoardDetails', function () {
    return view('boarddetails');
})->middleware(["auth","Verify"]);

Route::any('StartGame', 'StartGameController@game')->middleware(["auth","Verify"]);
Route::any('MovePiece', 'NewMovePiece@piece')->middleware(["auth","Verify"]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware("Verify");
Route::get('verify',function (){
    return view('verification');
});
Route::get('admin',function (){
    return view('AdminRights');
});
Route::get('static',function (){
    return view('staticPage');
})->middleware(['auth','Verify','isAdmin']);
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');

Route::any('start','GameController@start')->middleware(['auth','Verify']);
Route::any('move','GameController@movePiece')->middleware(['auth','Verify']);