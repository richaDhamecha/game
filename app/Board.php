<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{

    protected $table='boards';
    protected $fillable = ['id','rows', 'columns', 'game_id',];

    public function board_pieces()
    {
        return $this->hasMany('App\BoardPiece');
    }
    public function moves()
    {
        return $this->hasMany('App\Move');
    }

    public function games()
    {
        return $this->belongsTo('App\Game');
    }

}
