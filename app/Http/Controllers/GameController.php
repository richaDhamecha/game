<?php

namespace App\Http\Controllers;

use App\Board;
use App\Game;
use App\Move;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GameController extends Controller
{
    public function start(Request $request)
    {

        $user = Auth::user();
        $game = Game::where([['user_id', $user->id], ['is_active', true]])->first();
        if (!$game) {
            return redirect('BoardDetails');
        }
        $board = $game->boards()->first();
        $boardsPieces = $board->board_pieces()->get();
        if ($boardsPieces->isEmpty()) {
            $game->is_active = false;
            $game->save();
            return redirect('start');
        }
        $refresh = false;
        $errorMsg = '';

        return view('display_board', ['gameId' => $game->id, 'boardsPieces' => $boardsPieces, 'refresh' => $refresh, 'errorMsg' => $errorMsg, 'board' => $board]);

    }

    public function movePiece(Request $request)
    {
        $gameId = $request->input('GameId');
        $game = Game::where('id', $gameId)->first();
        $board = $game->boards()->first();
        $boardsPieces = $board->board_pieces()->where([['commands', '!=', '']])->get();
        $refresh = false;
        $Invalid = false;
        $errorMsg = '';
        $lastBoardPiece = $board->board_pieces()->orderBy('id', 'desc')->first();
        $lastMove = $board->moves()->orderBy('id', 'desc')->first();
        if ($boardsPieces->isNotEmpty()) {
            if (($lastMove == null)) {
                $currentBoardPiece = $board->board_pieces()->first();
            } else if ($lastMove->piece_id == $lastBoardPiece->piece_id) {
                $currentBoardPiece = $board->board_pieces()->where([['commands', '!=', '']])->first();
            } else {
                $currentBoardPiece = $board->board_pieces()->where([['id', '>', $board->board_pieces()->where('piece_id', $lastMove->piece_id)->value('id')], ['commands', '!=', '']])->first();
                if ($currentBoardPiece == null) {
                    $currentBoardPiece = $board->board_pieces()->where([['commands', '!=', '']])->first();
                }
            }

            if ($currentBoardPiece) {
                $currentPiece = $board->board_pieces()->where('id', $currentBoardPiece->id)->first();
                $moves = explode(',', $currentPiece->commands);
                $move = array_shift($moves);
                $currentPiece->commands = implode(',', $moves);
                $currentPiece->save();
                if ($move != '') {
                    $currentPieceId = $currentPiece->piece_id;
                    $currentPieceRow = $currentPiece->x;
                    $currentPieceColumn = $currentPiece->y;
                    $otherPieces = $board->board_pieces()->where('id', '!=', $currentBoardPiece->id)->get();
                    $isInvalidMove = false;
                    switch ($move) {

                        case "up":

                            foreach ($otherPieces as $otherPiece) {
                                if (($currentPieceRow - 1 == $otherPiece->x) && $currentPieceColumn == $otherPiece->y) {
                                    $isInvalidMove = true;
                                    break;
                                }
                            }
                            if (($currentPieceRow == 1) || $isInvalidMove == true) {
                                $Invalid = true;
                                $errorMsg = 'Up Move not valid';

                            } else {
                                $currentPieceRow = $currentPieceRow - 1;

                            }
                            break;

                        case "down":

                            foreach ($otherPieces as $otherPiece) {
                                if (($currentPieceRow + 1 == $otherPiece->x) && $currentPieceColumn == $otherPiece->y) {
                                    $isInvalidMove = true;
                                    break;
                                }
                            }

                            if (($currentPieceRow == $board->rows) || $isInvalidMove == true) {
                                $Invalid = true;
                                $errorMsg = 'Down Move not valid';
                            } else {
                                $currentPieceRow = $currentPieceRow + 1;
                            }
                            break;

                        case "left":
                            foreach ($otherPieces as $otherPiece) {
                                if (($currentPieceRow == $otherPiece->x) && $currentPieceColumn - 1 == $otherPiece->y) {
                                    $isInvalidMove = true;
                                    break;
                                }
                            }

                            if (($currentPieceColumn == 1) || $isInvalidMove == true) {
                                $Invalid = true;
                                $errorMsg = 'Left Move not valid';
                            } else {
                                $currentPieceColumn = $currentPieceColumn - 1;
                            }
                            break;

                        case "right":
                            foreach ($otherPieces as $otherPiece) {
                                if (($currentPieceRow == $otherPiece->x) && $currentPieceColumn + 1 == $otherPiece->y) {
                                    $isInvalidMove = true;
                                    break;
                                }
                            }

                            if (($currentPieceColumn == $board->columns) || $isInvalidMove == true) {
                                $Invalid = true;
                                $errorMsg = 'Right Move not valid';
                            } else {
                                $currentPieceColumn = $currentPieceColumn + 1;
                            }
                            break;

                        default:
                            break;
                    }
                    $currentPiece->x = $currentPieceRow;
                    $currentPiece->y = $currentPieceColumn;
                    $currentPiece->save();

                    if ($Invalid == true) {
                        $currentPiece->commands = '';
                        $currentPiece->save();
                    }

                    $moves = new Move();
                    $moves->board_id = $board->id;
                    $moves->piece_id = $currentPieceId;
                    $moves->command = $move;
                    $moves->save();

                    $refresh = true;
                    $board = Board::where('id', $board->id)->first();
                    $boardsPieces = $board->board_pieces()->get();
                    return view('display_seed', ['gameId' => $game->id, 'boardsPieces' => $boardsPieces, 'refresh' => $refresh, 'errorMsg' => $errorMsg, 'board' => $board]);
                }
            }
        } else {
            $game->is_active = false;
            $game->save();
            $errorMsg = 'No More Moves. Start a new game';

        }
        $board = Board::where('id', $board->id)->first();
        $boardsPieces = $board->board_pieces()->get();
        return view('display_board', ['gameId' => $game->id, 'boardsPieces' => $boardsPieces, 'refresh' => $refresh, 'errorMsg' => $errorMsg, 'board' => $board]);

    }

}
