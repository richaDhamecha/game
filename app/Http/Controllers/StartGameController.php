<?php
namespace App\Http\Controllers;
use App\Board;
use App\BoardPiece;
use App\Game;
use App\Move;
use App\Piece;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class StartGameController extends Controller
{

    public function game(Request $request)
    {
        $rows = $request->input('rows');
        $columns = $request->input('columns');
        $noOfPieces = $request->input('no_of_pieces');
        $errorMsg='';
        $refresh=false;
        $user=Auth::user();

        $game=new Game();
        $game->is_active = true;
        $game->user_id=$user->id;
        $game->save();
        $gameId = $game->id;


        $board =new Board();
        $board->rows = $rows;
        $board->columns= $columns;
        $board->game_id = $gameId;
        $board->save();
        $boardId = $board->id;


        for ($i = 0; $i < $noOfPieces; $i++) {
            $pieceRow = $request->input('piece' . $i . 'row');
            $pieceColumn = $request->input('piece' . $i . 'column');
            $commands = $request->input('piece' . $i . 'moves');

            $flag=false;
            if ($pieceRow <= $rows && $pieceColumn <= $columns) {
                $pieces = Piece::all();
                foreach ($pieces as $piece) {
                    if ($pieceRow == $piece->row && $pieceColumn == $piece->column) {
                        $oldPieceId = $piece->id;
                        $flag = true;
                        break;
                    }
                }
                if ($flag == true) {
                    $pieceId = $oldPieceId;
                } else {

                    $newpiece = new Piece;
                    $newpiece->row = $pieceRow;
                    $newpiece->column = $pieceColumn;
                    $newpiece->save();
                    $pieceId = $newpiece->id;
                }

                $boardPieces = new BoardPiece();

                $boardPieces->board_id = $boardId;
                $boardPieces->piece_id = $pieceId;
                $boardPieces->x = $pieceRow;
                $boardPieces->y = $pieceColumn;
                $boardPieces->commands = $commands;
                $boardPieces->save();
            }
            else {
                echo"<script type='text/javascript'>
                        alert('Enter valid values');history.go(-1)</script>";
                break;
            }

        }
        $board = Board::where('id', $board->id)->first();
        $boardsPieces = $board->board_pieces()->get();
        return view('display_board', ['gameId' => $game->id, 'boardsPieces' => $boardsPieces, 'refresh' => $refresh, 'errorMsg' => $errorMsg, 'board' => $board]);
    }


}