<?php

namespace App\Http\Controllers;

use App\BoardPiece;
use App\Game;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StartSeed extends Controller
{
    public function start(Request $request)
    {

        $user = Auth::user();
        $game = Game::where([['user_id', $user->id], ['is_active', true]])->first();
        if (!$game) {
            return redirect('BoardDetails');
        }
        $board = $game->boards()->first();
        $boardsPieces = $board->board_pieces()->get();
        if ($boardsPieces->isEmpty()) {
            $game->is_active = false;
            $game->save();
            return redirect('start');

        }
        $refresh = false;
        $errorMsg = '';

        return view('display_seed', ['gameId' => $game->id, 'boardsPieces' => $boardsPieces, 'refresh' => $refresh, 'errorMsg' => $errorMsg, 'board' => $board]);

    }
}
