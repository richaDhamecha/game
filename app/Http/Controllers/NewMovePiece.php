<?php

namespace App\Http\Controllers;

use App\Board;
use App\BoardPiece;
use App\Game;
use App\Move;
use App\Piece;
use Illuminate\Http\Request;


class NewMovePiece extends Controller
{
    public function piece(Request $request)
    {
        $noOfPieces = $request->input('noOfPieces');
        $boardPieceIds = $request->input('BoardPiece');
        $boardId = $request->input('BoardId');
        $gameId = $request->input('GameId');

        $board = Board::where('id', $boardId)->first();

        $boardRows = $board->rows;
        $boardColumns = $board->columns;
        $currentBoardPieceId=0;
        $refresh = false;
        $Invalid = false;
        $errorMsg = '';
        $lastBoardPiece = BoardPiece::where('board_id', $boardId)->orderBy('id','desc')->first();
        $lastMove = Move::where('board_id', $boardId)->orderBy('id','desc')->first();
        if(($lastMove==null))
        {
            $boardPiece = BoardPiece::where('board_id', $boardId)->first();
            $currentBoardPieceId=$boardPiece->id;

        }
        elseif ($lastMove->piece_id == $lastBoardPiece->piece_id) {
            foreach ($boardPieceIds as $boardPieceId)
            {
                $boardPiece = BoardPiece::where('id', $boardPieceId)->first();
                if($boardPiece->commands){
                    $currentBoardPieceId=$boardPiece->id;
                    break;
                }
            }
        }
        else
        {
            $lastPieceId=$lastMove->piece_id;
            $boardPiece=BoardPiece::where([['piece_id',$lastPieceId],['board_id',$boardId]])->first();
            $currentBoardPiece=BoardPiece::where([['id','>',$boardPiece->id],['commands','!=','']])->first();
            $currentBoardPieceId=$currentBoardPiece->id;
        }

        if($currentBoardPieceId!=0) {


            $currentPiece = BoardPiece::where('id', $currentBoardPieceId)->first();
            $currentCommands = $currentPiece->commands;
            $moves = explode(',', $currentCommands);
            $move = array_shift($moves);
            $x = implode(',', $moves);
            $currentPiece->commands = $x;
            $currentPiece->save();

            if ($move != '') {
                $currentPieceId = $currentPiece->piece_id;
                $currentPieceRow = $currentPiece->x;
                $currentPieceColumn = $currentPiece->y;
                $otherPieces = BoardPiece::whereIn('id', $boardPieceIds)->where('id', '!=', $currentBoardPieceId)->get();

                $isInvalidMove = false;
                switch ($move) {

                    case "up":

                        foreach ($otherPieces as $otherPiece) {
                            if (($currentPieceRow - 1 == $otherPiece->x) && $currentPieceColumn == $otherPiece->y) {
                                $isInvalidMove = true;
                                break;
                            }
                        }
                        if (($currentPieceRow == 1) || $isInvalidMove == true) {
                            $Invalid = true;
                            $errorMsg = 'Up Move not valid';

                        } else {
                            $currentPieceRow = $currentPieceRow - 1;

                        }
                        break;

                    case "down":

                        foreach ($otherPieces as $otherPiece) {
                            if (($currentPieceRow + 1 == $otherPiece->x) && $currentPieceColumn == $otherPiece->y) {
                                $isInvalidMove = true;
                                break;
                            }
                        }

                        if (($currentPieceRow == $boardRows) || $isInvalidMove == true) {
                            $Invalid = true;
                            $errorMsg = 'Down Move not valid';
                        } else {
                            $currentPieceRow = $currentPieceRow + 1;
                        }
                        break;

                    case "left":
                        foreach ($otherPieces as $otherPiece) {
                            if (($currentPieceRow == $otherPiece->x) && $currentPieceColumn - 1 == $otherPiece->y) {
                                $isInvalidMove = true;
                                break;
                            }
                        }

                        if (($currentPieceColumn == 1) || $isInvalidMove == true) {
                            $Invalid = true;
                            $errorMsg = 'Left Move not valid';
                        } else {
                            $currentPieceColumn = $currentPieceColumn - 1;
                        }
                        break;

                    case "right":
                        foreach ($otherPieces as $otherPiece) {
                            if (($currentPieceRow == $otherPiece->x) && $currentPieceColumn + 1 == $otherPiece->y) {
                                $isInvalidMove = true;
                                break;
                            }
                        }

                        if (($currentPieceColumn == $boardColumns) || $isInvalidMove == true) {
                            $Invalid = true;
                            $errorMsg = 'Right Move not valid';
                        } else {
                            $currentPieceColumn = $currentPieceColumn + 1;
                        }
                        break;

                    default:
                        break;
                }
                $currentPiece->x = $currentPieceRow;
                $currentPiece->y = $currentPieceColumn;
                $currentPiece->save();


                if ($Invalid == true) {
                    $currentPiece->commands = '';
                    $currentPiece->save();
                }
                $moves = new Move();
                $moves->board_id = $boardId;
                $moves->piece_id = $currentPieceId;
                $moves->command = $move;
                $moves->save();
                $refresh = true;

                $pieceId = [];

                $boardPieces = BoardPiece::whereIn('id', $boardPieceIds)->get();

                foreach ($boardPieces as $boardPiece) {
                    $boardPiecesIds[] = $boardPiece->id;
                    $pieceId[] = $boardPiece->piece_id;
                    $pieceRows[] = $boardPiece->x;
                    $pieceColumns[] = $boardPiece->y;
                }
                return view('display', ['rows' => $boardRows, 'columns' => $boardColumns, 'piecesRows' => $pieceRows, 'piecesColumns' => $pieceColumns, 'pieceID' => $pieceId, 'gameId' => $gameId, 'boardId' => $boardId, 'boardPiece' => $boardPiecesIds, 'noOfPieces' => $noOfPieces, 'refresh' => $refresh, 'error' => $Invalid, "errorMsg" => $errorMsg]);
            }
        }



        $boardPieces = BoardPiece::whereIn('id',$boardPieceIds)->get();

        foreach ($boardPieces as $boardPiece) {
            $boardPiecesIds[] = $boardPiece->id;
            $pieceId[] = $boardPiece->piece_id;
            $pieceRows[] = $boardPiece->x;
            $pieceColumns[] = $boardPiece->y;
        }
        return view('display', ['rows' => $boardRows, 'columns' => $boardColumns, 'piecesRows' => $pieceRows, 'piecesColumns' => $pieceColumns, 'pieceID' => $pieceId, 'gameId' => $gameId, 'boardId' => $boardId, 'boardPiece' => $boardPiecesIds, 'noOfPieces' => $noOfPieces,'refresh'=>$refresh,'error'=>$Invalid,"errorMsg"=>$errorMsg]);
    }

}