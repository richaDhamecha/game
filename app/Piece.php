<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Piece extends Model
{
    protected $table='pieces';
    protected $fillable = ['id','row','column'];



    public function board_pieces()
    {
        return $this->hasMany('App\BoardPiece');
    }
    public function moves()
    {
        return $this->hasMany('App\Move');
    }
}
