<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Move extends Model
{

    use SoftDeletes;

    protected $table='moves';
    protected $fillable = ['id','board_id', 'piece_id', 'command',];
    protected $dates = ['deleted_at'];


    public function boards()
    {
        return $this->belongsTo('App\Board');
    }

    public function pieces()
    {
        return $this->belongsTo('App\Piece');
    }

}
