<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table='games';
    protected $fillable = ['id','is_active'];


    public function boards()
    {
        return $this->hasOne('App\Board');
    }
    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
