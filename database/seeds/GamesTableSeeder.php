<?php

use App\User;
use Illuminate\Database\Seeder;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users=User::all();
        foreach ($users as $user) {
            $userIds[] = $user->id;
        }
        for ($k = 0; $k < 3; $k++) {
            $n = mt_rand(1, 4);
            shuffle($userIds);
            for ($j = 0; $j < $n; $j++) {
                $randomUserIds[] = $userIds[$j];
            }
            foreach ($randomUserIds as $randomUserId) {
                factory(App\Game::class)->create(['user_id' => $randomUserId]);

            }
        }
    }
}
