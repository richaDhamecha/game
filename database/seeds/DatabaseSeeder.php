<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            UsersTableSeeder::class,
            GamesTableSeeder::class,
            BoardTableSeeder::class,
            BoardPieceTableSeeder::class,
        ]);
    }
}
