<?php

use App\Game;
use Illuminate\Database\Seeder;

class BoardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $games=Game::all();
        foreach ($games as $game)
        {
            factory(App\Board::class)->create(['game_id' => $game->id]);
        }

    }
}
