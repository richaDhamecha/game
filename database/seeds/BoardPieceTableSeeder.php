<?php

use App\Board;
use App\Piece;
use Illuminate\Database\Seeder;

class BoardPieceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $boards=Board::all();
        foreach ($boards as $board)
        {
            $noOfPieces = mt_rand(1, 5);

            for ($i = 0; $i < $noOfPieces; $i++) {
                $x = rand(1, $board->rows);
                $y = rand(1, $board->columns);

                $boardPieces = $board->board_pieces()->where([['x', $x], ['y', $y]])->first();

                if ($boardPieces == null) {
                    $piece = Piece::where([['row', $x], ['column', $y]])->first();
                    if ($piece != null) {
                        factory(App\BoardPiece::class)->create(['piece_id' => $piece->id, 'board_id' => $board->id]);
                    } else {
                        $newpiece = new Piece();
                        $newpiece->row = $x;
                        $newpiece->column = $y;
                        $newpiece->save();
                        factory(App\BoardPiece::class)->create(['piece_id' => $newpiece->id, 'board_id' => $board->id]);
                    }
                } else {
                    $i = $i - 1;
                }

            }


        }
    }
}
