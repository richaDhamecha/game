<?php

use Faker\Generator as Faker;

$factory->define(App\BoardPiece::class, function (Faker $faker) {
    $array=['up','down','left','right'];
    $n=mt_rand(1,4);
    shuffle($array);
    for($j=0;$j<$n;$j++)
    {
        $random[]=$array[$j];
    }
    $commands=implode(',',$random);
    return [
        'x'=>function (array $boardPiece) {
            return App\Piece::find($boardPiece['piece_id'])->row;
        },
        'y'=>function (array $boardPiece) {
            return App\Piece::find($boardPiece['piece_id'])->column;
        },
        'commands'=>$commands,
    ];
});
