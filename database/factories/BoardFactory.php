<?php

use Faker\Generator as Faker;

$factory->define(App\Board::class, function (Faker $faker) {
    return [
        'rows'=>rand(3,10),
        'columns'=>rand(3,10),

    ];
});
