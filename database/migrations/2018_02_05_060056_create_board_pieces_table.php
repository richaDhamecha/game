<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardPiecesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board_pieces', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('board_id')->unsigned();
            $table->integer('piece_id')->unsigned();
            $table->integer('x');
            $table->integer('y');
            $table->string('commands');
            $table->timestamps();
        });
        Schema::table('board_pieces',function (Blueprint $table){
           $table->foreign('board_id')->references('id')->on('boards');
           $table->foreign('piece_id')->references('id')->on('pieces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('board_pieces');
    }
}
